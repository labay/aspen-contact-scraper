# Mac Usage Instructions

## Step 0: Install Google Chrome

In the *highly unlikely* event that you don't have Google Chrome installed as your browser, go to [chrome.google.com](https://chrome.google.com) and get it.

## Step 1: Download the latest release
Navigate to the [Releases](https://gitlab.com/labay/aspen-contact-scraper/-/releases) page.
[<img src="images/releases.png" width="200 "/>](images/releases.png)

Download the file named `aspen-contact-scraper-darwin-amd64-[version].zip`

## Step 2: Extract the `.zip` archive
In your _Downloads_ folder, double-click on the `.zip` file you downloaded. It will extract itself, and create a folder named `dist`.
[<img src="images/usage_mac/dist_folder.png" width="200 "/>](images/usage_mac/dist_folder.png)

## Step 3: Navigate to the executable
Open the `dist` folder. Inside it will be a `macos` folder. Open it.
[<img src="images/usage_mac/macos_folder.png" width="200 "/>](images/usage_mac/macos_folder.png)

## Step 4: Run the app
Double-click on `contactscrape`.
[<img src="images/usage_mac/contactscrape_app.png" width="200 "/>](images/usage_mac_/contactscrape_app.png)

## Step 5: Collect your contats

There should now be a file named `contacts.csv` in the folder, ready for use. [<img src="images/usage_windows/contacts.csv.png" width="200 "/>](images/usage_windows/contacts.csv.png)