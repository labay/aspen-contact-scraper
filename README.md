# Aspen Contact Scraper

Scrape all your students' contacts from Aspen, and package them for quick import into your contact book.

## Brief Usage Instructions
```bash
usage: contactscrape.py [-h] [--username USERNAME] [--password PASSWORD] [--url URL] [--verbosity VERBOSITY]

Pull student contacts from Aspen.

options:
  -h, --help            show this help message and exit
  --username USERNAME   Your Aspen username
  --password PASSWORD   Your Aspen password. If it contains special characters, enclose it in quotes (e.g. 'C@$hM0n3Y')
  --url URL             URL of your Aspen instance. Probably ends in /aspen
  --verbosity VERBOSITY
                        Level of verbosity; defaults to ERROR
```


## Changelog

### 2023-09-02 - 2.0.0
#### Changed
-   Requests instead of Selenium

### 2022-12-29 - 1.1.0
#### Added
-   Binaries for Windows and MacOS
-   Output to Google Contacts

#### Changed
-   Use of Questionary for prompts, eliminating the need for command-line arguments
-   Download a CSV file instead of loading displayed text

### 2022-09-24 - 1.0.1
#### Bugfixes
-   Correct out-of-control page-skipping after Page 1

### 2022-09-24 - 1.0.0
Initial release.

#### Added
-   Parameters for non-DCPS URL and verbosity

#### Changed
-   Standard outputs are printed instead of relying on logging
-   Takes advantage of the fact that students are sorted alphabetically to not reset the search page after every student. This speeds things up dramatically toward the end of the list.

### 2022-09-22 - 0.9.0
Pre-release version. Works only with DCPS and has some inefficiencies.