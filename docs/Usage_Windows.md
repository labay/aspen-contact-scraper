# Windows Usage Instructions

## Step 0: Install Google Chrome

In the *highly unlikely* event that you don't have Google Chrome installed as your browser, go to [chrome.google.com](https://chrome.google.com) and get it.

## Step 1: Download the latest release
Navigate to the [Releases](https://gitlab.com/labay/aspen-contact-scraper/-/releases) page.
[<img src="images/releases.png" width="200 "/>](images/releases.png)

Download the file named `aspen-contact-scraper-windows-amd64-[version].zip`

## Step 2: Extract the `.zip` archive
In your _Downloads_ folder, right-click on the `.zip` file you downloaded in Step 1. Click on **Extract All...** and pick where you want the folder extracted.
[<img src="images/usage_windows/extract_all.png" width="200 "/>](images/usage_windows/extract_all.png)

## Step 3: Navigate to the executable
Open the folder you just extracted. Inside will be a `dist` folder. Open it.
[<img src="images/usage_windows/dist_folder.png" width="200 "/>](images/usage_windows/dist_folder.png)

Inside that will be a `windows` folder. Open it.
[<img src="images/usage_windows/windows_folder.png" width="200 "/>](images/usage_windows/windows_folder.png)

## Step 4: Run the app
Double-click on `contactscrape.exe`.
[<img src="images/usage_windows/contactscrape_exe.png" width="200 "/>](images/usage_windows/contactscrape_exe.png)

When prompted, click **Run Anyway**.
[<img src="images/usage_windows/run_anyway.png" width="200 "/>](images/usage_windows/run_anyway.png)

## Step 5: Collect your contats

There should now be a file named `contacts.csv` in the folder, ready for use. [<img src="images/usage_mac/contacts.csv.png" width="200 "/>](images/usage_mac/contacts.csv.png)