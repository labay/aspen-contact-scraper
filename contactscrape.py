from bs4 import BeautifulSoup
from requests_html import HTMLSession, AsyncHTMLSession
import re
import pandas as pd
import numpy as np
import questionary
import argparse
import progressbar
import logging

def Spacer(n=2):
    for i in range(n):
        print('')

class Session:
    def __init__(self, url, username, password, logger=None):
        self.baseurl = self.PrepBaseURL(url)
        self.username, self.password = self.LoginInfo(username, password)
        self.session = self.Instantiate()
        self.logger = logger if logger else logging.getLogger('aspen')

        self.deploymentId = None

    def PrepBaseURL(self, url):
        # Check if the URL has an httpx prefix. If so, we win. Else, add https://
        e = re.compile('https*\:\/\/')
        if not e.match(url) and url[-1] != '/':
            return 'https://' + url + '/'
        elif not e.match(url):
            return 'https://' + url
        elif url[-1] != '/':
            return url + '/'
        else:
            return url

    def LoginInfo(self, username, password):
        if not username:
            username = questionary.text("What is your Aspen username?").ask()

        if not password:
            password = questionary.password("What is your Aspen password?").ask()

        return username, password
    
    def Instantiate(self):
        session = HTMLSession()

        return session
    
    def login(self):
        print('Logging in')
        Spacer(1)
        url = self.baseurl + 'logon.do'
        r, soup, token, cookies = Page(self, url).Get()

        # Get Deployment ID
        self.deploymentId = re.compile('(document\.forms\[0\]\.elements\[\'deploymentId\'\]\.value \= )(.*)\;').search(r.text).group(2)[1:-1]

        payload = {'username': self.username, 'password': self.password, 'districtId': '*dst', 'deploymentId': self.deploymentId, 'userEvent': 930, 'org.apache.struts.taglib.html.TOKEN': token, 'userParam': None, 'operationId': None, 'scrollX': 0, 'scrollY': 0, 'formFocusField': 'username', 'mobile': False, 'SSOLoginDone': None}
        #r = self.session.post(url, data=payload, cookies=cookies)
        r, soup, token, cookies = Page(self, url, payload=payload, cookies=cookies).Post()

        text = r.text
        e = re.compile('(var msg \=)(.*)(\;)')
        if e.search(text):
            msg = e.search(text).group(2)
            print('Oh dear. Aspen said: %s'%msg)
            return False
        else:
            return True
        
class Page:
    def __init__(self, Session, url, payload=None, cookies=None):
        self.Session = Session
        self.session = Session.session
        self.logger = Session.logger
        self.baseurl = Session.baseurl
        self.payload = payload
        self.cookies = cookies
        self.url = self.PrepURL(url)

    def PrepURL(self, url):
        # If we get a full URL, proceed. If we get a stub, attach baseurl.
        e = re.compile('https*\:\/\/')
        if e.match(url):
            url = url
        else:
            url = self.baseurl + url

        return url
    
    def Get(self):
        self.logger.info('Loading %s'%self.url)
        r = self.session.get(self.url)

        soup = BeautifulSoup(r.text, 'html.parser')
        token = soup.find('input', attrs={'name': 'org.apache.struts.taglib.html.TOKEN'}).attrs['value']
        cookies = r.cookies

        return r, soup, token, cookies
    
    def Post(self):
        self.logger.info('Loading %s'%self.url)
        if self.cookies:
            r = self.session.post(self.url, cookies=self.cookies, data=self.payload)
        else:
            r = self.session.post(self.url, data=self.payload)

        soup = BeautifulSoup(r.text, 'html.parser')
        token = soup.find('input', attrs={'name': 'org.apache.struts.taglib.html.TOKEN'}).attrs['value']
        cookies = r.cookies

        return r, soup, token, cookies

class Roster:
    def __init__(self, session):
        self.session = session
        studentiter, self.token = self.GetStudents()
        self.students = studentiter
    
    def GetStudents(self):
        print('Getting students...')
        studentlisturl = self.session.baseurl + 'staffStudentList.do?navkey=student.std.list'
        r, soup, token, cookies = Page(self.session, studentlisturl).Get()

        # Get the number of pages
        pagelisting = soup.find('select', attrs={'id': 'listHeaderDropdown'})
        pagelisting.find_all('option')
        e = re.compile('^\d+')
        maxpage = int(e.search([o.text for o in pagelisting][-1]).group(0))

        # Set up the progress bar
        widgets = [progressbar.Percentage(), progressbar.Bar()]
        bar = progressbar.ProgressBar(widgets=widgets, max_value=maxpage).start()

        # Scrape each page
        Students = []
        i = 1
        fieldsetoid = None
        while i <= maxpage:
            #print('Page %d'%i)
            bar.update(i)
            fieldsetoid = soup.find('input', attrs={'name': 'fieldSetOid'}).attrs['value']

            students = pd.read_html(r.text, match='Pupil ID', flavor='bs4', header=0)[2]
            cols = ['Pupil ID', 'Name']
            students = students[cols]

            # Split the student names
            students['Last Name'] = students['Name'].apply(lambda x: re.compile('^([^\,]+)(\, )(.*)$').search(x).group(1))
            students['First Name'] = students['Name'].apply(lambda x: re.compile('^([^\,]+)(\, )(.*)$').search(x).group(3))

            # Gather other metadata
            tds = r.html.find('td')
            ids = [t.attrs['id'] for t in tds if 'id' in t.attrs.keys()]
            e = re.compile('^[Ss][Tt][Dd].*')
            ids = [e.search(i).group(0) for i in ids if e.search(i)]
            students['id'] = ids
            students['fieldSetOid'] = fieldsetoid

            Students.append(students)

            i += 1
            if i <= maxpage:
                payload = {'org.apache.struts.taglib.html.TOKEN': token, 'userEvent': 10, 'userParam': None, 'operationId': None, 'deploymentId': self.session.deploymentId, 'scrollX': 0, 'scrollY': 0, 'formFocusField': None, 'formContents': None, 'formContentsDirty': None, 'maximized': False, 'menubarFindInputBox': None, 'jumpToSearch': None, 'topPageSelected': i, 'allowMultipleSelection': True, 'scrollDirection': None, 'fieldSetName': '+Next+Homeroom-ELEM', 'fieldSetOid': fieldsetoid, 'filterDefinitionId': '###classes', 'basedOnFilterDefinitionId': None, 'filterDefinitionName': 'filter.staffMyClasses', 'sortDefinitionId': 'default', 'sortDefinitionName': 'Name', 'editColumn': None, 'editEnabled': False, 'runningSelection': None}
                listurl = self.session.baseurl + 'staffStudentList.do'
                r, soup, token, cookies = Page(self.session, listurl, cookies=cookies, payload=payload).Post()
        Students = pd.concat(Students).reset_index()
        bar.finish()
        print('...found %d students'%len(Students))
        Spacer(1)

        return Students, token
    
    def GetContacts(self):
        print('Harvesting contacts')
        Contacts = []

        # Set up the progress bar
        widgets = [progressbar.Percentage(), progressbar.Bar(), progressbar.FormatLabel('')]
        bar = progressbar.ProgressBar(widgets=widgets, max_value=len(self.students)).start()

        for index, s in self.students.iterrows():
            widgets[2] = progressbar.FormatLabel(s['Name'])
            bar.update(index)

            S = Student(self.session, self.token, s)
            Contacts.append(S.contacts)
        Contacts = pd.concat(Contacts)
        bar.finish()

        return Contacts

class Student:
    def __init__(self, session, token, student):
        self.session = session
        self.token = token
        self.student = student
        self.contacts = self.GetContacts()
    
    def GetContacts(self):
        studentlisturl = self.session.baseurl + 'staffStudentList.do?navkey=student.std.list'
        r, soup, token, cookies = Page(self.session, studentlisturl).Get()
        payload = {'org.apache.struts.taglib.html.TOKEN': self.token, 'userEvent': 2100, 'userParam': self.student['id'], 'operationId': None, 'deploymentId': self.session.deploymentId, 'scrollX': 0, 'scrollY': 0, 'formFocusField': None, 'formContents': None, 'formContentsDirty': None, 'maximized': False, 'menubarFindInputBox': None, 'jumpToSearch': None, 'topPageSelected': 0, 'allowMultipleSelection': True, 'scrollDirection': None, 'fieldSetName': '+Next+Homeroom-ELEM', 'fieldSetOid': self.student['fieldSetOid'], 'filterDefinitionId': None, 'filterDefinitionName': 'filter.staffMyClasses', 'sortDefinitionId': 'default', 'sortDefinitionName': 'Name', 'editColumn': None, 'editEnabled': False, 'runningSelection': None}
        studentdetailurl = self.session.baseurl + 'staffStudentList.do'
        r, soup, token, cookies = Page(self.session, studentdetailurl, payload=payload).Post()
        contacturl = self.session.baseurl + 'contextList.do?navkey=student.std.list.con'
        r, soup, token, cookies = Page(self.session, contacturl).Get()

        contacts = pd.read_html(r.text, match='Primary email', flavor='bs4', header=0)[2].fillna('')
        cols = ['Name', 'Relationship', 'HmePh', 'CellPh', 'Primary email', 'Contact', 'User', 'Priority']
        bools = ['Contact', 'User']
        contacts = contacts[cols]
        for b in bools:
            contacts[b] = contacts[b].apply(lambda x: x == 'Y')
        contacts['Pupil ID'] = self.student['Pupil ID']
        contacts['Student First Name'] = self.student['First Name']
        contacts['Student Last Name'] = self.student['Last Name']

        contacts = contacts[contacts['Contact'] == True]

        # Split contact's name
        e = re.compile('^([^\,]+)(\, )(.*)$')
        contacts['Last Name'] = contacts['Name'].apply(lambda x: e.search(x).group(1))
        contacts['First Name'] = contacts['Name'].apply(lambda x: e.search(x).group(3))
        
        # Rearrange columns
        contacts = contacts[['Pupil ID', 'Student Last Name', 'Student First Name', 'Priority', 'Last Name', 'First Name', 'Relationship', 'HmePh', 'CellPh', 'Primary email']]
        return contacts

def ScrapeContacts(args):
    # Pull the arguments
    username = args.username
    password = args.password
    verbosity = args.verbosity

    # Set up the basics
    format = '%(message)s'
    verbositylevels = {'CRITICAL': logging.CRITICAL,
                       'ERROR': logging.ERROR,
                       'WARNING': logging.WARNING,
                       'INFO': logging.INFO,
                       'DEBUG': logging.DEBUG,
                       'NOTSET': logging.NOTSET}

    logging.basicConfig(level=verbositylevels[verbosity])
    logger = logging.getLogger('contacts')

    # If no URL provided as argument, ask.
    url = args.url
    if not url:
        url = questionary.text("What is the address of your Aspen server?", default='https://aspen.dcps.dc.gov/aspen').ask()

    # Instantiate the Session and log into Aspen
    S = Session(url, username, password)
    success = S.login()
    while not success:
        c = questionary.select("What do you want to do?",
            choices=[
                "Try again",
                "Quit"],
            default="Try again").ask()
        if c == "Quit":
            exit()
        elif c == "Try again":
            S = Session(url, username, password)
            success = S.login()

    # Switch to Staff context
    contexturl = S.baseurl + 'openContext.do?context=7'
    r, soup, token, cookies = Page(S, contexturl).Get()

    # Grab them contacts!
    contacts = Roster(S).GetContacts()
    contacts.to_csv('./contacts.csv', index=False)

    print('Your contacts have been saved as contacts.csv')
    
    return contacts

def OutputAction(AddressBooks):
    choices = list(AddressBooks.keys())
    
    print('Your output can also be packaged into various contact book types for quick import.')
    outputaction = questionary.select('How would you like your output packaged?', choices=choices, default='Do nothing').ask()

    return AddressBooks[outputaction]

def Google(Contacts):
    Template = pd.read_csv('./templates/googlecontacts.csv', header=0)

    # Extract unique Relationship values
    ContactTypes = pd.DataFrame(Contacts['Relationship'].drop_duplicates().reset_index(drop=True))

    # Prompt the user about which relationships they want to include
    Include = []
    Spacer(1)
    print('Often, people only want to include parental contacts, or some other subset of contacts.')
    print('\tFor each relationship type, specify whether you want to include or reject contacts with that type.')
    for index, r in ContactTypes.iterrows():
        Include.append(questionary.confirm('Include contacts of type "%s"?'%r['Relationship']).ask())
    ContactTypes['Include'] = Include

    # Join inclusion flags and filter
    ContactTypes = ContactTypes[ContactTypes['Include'] == True]
    ContactTypes.set_index('Relationship', inplace=True)
    Contacts['key'] = Contacts['Relationship']
    Contacts.set_index('key', inplace=True)
    Contacts = Contacts.join(ContactTypes, how='inner')

    # Prioritize contacts
    Spacer(1)
    print('You can include as few or as many of each student\'s contacts as you would like, though they will always be taken in order of priority.')
    print('\tBy default, this app captures each student\'s top two contacts. \n\tTo capture all contacts, enter "All" in the next prompt.')
    e = re.compile('([Aa]ll)*\d*')
    FilteredContacts = pd.DataFrame()
    while len(FilteredContacts) == 0:
        N = questionary.text('How many contacts should be included per student?', \
            default='2', \
            validate=lambda text: True if e.search(text) and len(text) > 0 else 'Please enter "All" or a number.').ask()
        e = re.compile('\d+')
        if e.search(N) and int(N) == 0:
            print('Including 0 contacts seems like a poor choice. Shall we try again?')
        elif e.search(N):
            FilteredContacts = Contacts[Contacts['Priority'] <= int(N)]
            n = int(N)
        elif n.lower() == 'All':
            FilteredContacts = Contacts
            n = max(FilteredContacts.groupby(by='Pupil ID')['Priority'].max())
        else:
            print('Somehow, you have entered a value that appeared valid, but in fact was not. \
                Bravo for that, but you\'re going to have to enter a correct value to proceed.')

    # Add the appropriate headers to the template
    ColumnPrototypes = ['E-mail', 'Phone']
    ColumnTypes = ['Type', 'Value']
    for i in range(1, n):
        for j in ColumnPrototypes:
            for k in ColumnTypes:
                colname = j + ' ' + str(i) + ' - ' + k
                Template[colname] = np.nan

    # Decide how to handle different phone types
    Spacer(1)
    print('Each contact gets exactly one phone number.\n\tBy default, this app prioritizes cell phones over home phones when both are present.')
    phonenumberprioritychoices = ['Prioritize cell numbers', 'Prioritize home numbers']
    phonenumberpriority = questionary.select('How should phone types be handled?', \
        choices=phonenumberprioritychoices, \
        default=phonenumberprioritychoices[0]).ask()

    if phonenumberpriority == phonenumberprioritychoices[1]:
        phonedefault = 'home'
        phonefallback = 'cell'
    else:
        phonedefault = 'cell'
        phonefallback ='home'

    Spacer(1)
    phonefallback = questionary.confirm('If a contact lacks a %s phone, should the %s phone be substituted?'%(phonedefault, phonefallback)).ask()
    Spacer(1)
    nophone = questionary.confirm('If a contact has no phone numbers but does have an email address, should they still be included?').ask()

    # Template those contacts!
    GoogleContacts = []
    Students = Contacts['Pupil ID'].drop_duplicates().tolist()
    for s in Students:
        ContactRow = pd.Series([None for c in Template.columns], index=Template.columns).to_frame().T.fillna('')

        StudentContacts = Contacts[Contacts['Pupil ID'] == s].copy().reset_index()
        StudentContacts['Name'] = StudentContacts['Student First Name'] + ' ' + StudentContacts['Student Last Name']
        StudentContacts['Given Name'] = StudentContacts['Student First Name']
        StudentContacts['Family Name'] = StudentContacts['Student Last Name']
        
        for index, r in StudentContacts.iterrows():
            ContactRow['Name'] = r['Name']
            ContactRow['Given Name'] = r['Given Name']
            ContactRow['Family Name'] = r['Family Name']
            ContactRow['E-mail ' + str(index + 1) + ' - Type'] = r['Relationship'] + '(' + r['First Name'] + ' ' + r['Last Name'] + ')'
            ContactRow['E-mail ' + str(index + 1) + ' - Value'] = r['Primary email']
            ContactRow['Phone ' + str(index + 1) + ' - Type'] = r['Relationship'] + '(' + r['First Name'] + ' ' + r['Last Name'] + ')'
            if phonenumberpriority == phonenumberprioritychoices[0]:
                ContactRow['Phone ' + str(index + 1) + ' - Value'] = (r['HmePh'] if r['CellPh'] == '' and phonefallback else r['CellPh'])
            elif phonenumberpriority == phonenumberprioritychoices[1]:
                if phonefallback:
                    ContactRow['Phone ' + str(index + 1) + ' - Value'] = (r['CellPh'] if r['HomePh'] == '' and phonefallback else r['HomePh'])
            ContactRow = ContactRow.fillna('')
        
        ContactNumbers = [ContactRow.iloc[0]['Phone ' + str(i) + ' - Value'] for i in range(1, n) if ContactRow.iloc[0]['Phone ' + str(i) + ' - Value'] != '']
        if ContactNumbers == [] and not nophone:
            ContactRow = Template.copy()
            
        GoogleContacts.append(ContactRow)

    # Prep output
    GoogleContacts = pd.concat(GoogleContacts)
    GoogleContacts.to_csv('googlecontacts.csv', index=None)

    Spacer(1)
    print('Done! Your contacts are now available as googlecontacts.csv')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Pull student contacts from Aspen.')
    parser.add_argument('--username', type=str, help='Your Aspen username')
    parser.add_argument('--password', type=str, help='Your Aspen password. If it contains special characters, enclose it in quotes (e.g. \'C@$hM0n3Y\')')
    parser.add_argument('--url', type=str, help='URL of your Aspen instance. Probably ends in /aspen')
    #parser.add_argument('--skippull', type=bool, default=True)
    parser.add_argument('--verbosity', type=str, default='ERROR', help='Level of verbosity; defaults to ERROR')
    args = parser.parse_args()

    #if args.skippull:
    #    contacts = pd.read_csv('contacts.csv', header=0)
    #else:
    #    contacts = ScrapeContacts(args)
    contacts = ScrapeContacts(args)

    AddressBooks = {'Do nothing': None, \
                    'Google': Google}
    addressbook = OutputAction(AddressBooks)
    if addressbook:
        addressbook(contacts)